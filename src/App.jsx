import { About, DownloadButton, Education, Experience, Footer, Hero, LangSelector, More, Nav } from "./components";
import { useState, useContext } from "react";
import { CV } from "./CV/CV";
import { LangContext } from "./contexts/LangContext";





function App() {

  const {locale} = useContext(LangContext);
  const { hero, education, experience, back, skills, cap, tech, titles} = CV[locale] || CV['es'] ;
  
  const [showEducation, setShowEducation] = useState(true);

  return (
    <div className='global'>

      <Hero titles={titles} hero={hero} />
      <About  titles={titles}  hero={hero} />
      <DownloadButton />
      <Nav showEducation={showEducation}  setShowEducation={setShowEducation} />

      <div className="Toogle-section">
        {showEducation ? (
          <Education titles={titles}  education={education} />
        ) : (
          <Experience titles={titles}  experience={experience} />
        )}
      </div>

      <More titles={titles}   back={back} skills={skills} cap={cap} tech={tech} />
      <LangSelector />
      <Footer />
    </div>
  );
}

export default App;
