export const CV = {
  es: {
    titles:{
      about: "Sobre mi",
    },
    hero: {
      name: "Enrique ",
      subname: "Teruel Gutiérrez",
      city: "Murcia",
      email: "info@enriqueteruel.com",
      birthDate: "13/07/1991",
      phone: "(+34) 639880551",
      image: "/img/ojo.gif",
      gitHub: "@enriqueteru",
      title: "Branding | UX/UI | Programador Full Stack Junior",
      aboutMe: [
        {
          info: "Mis principales áreas de conocimiento son la comunicación corporativa, diseño y marketing, desde hace unos años estoy reciclándome para unir mis dos pasiones, el diseño y la programación.",
        },
        {
          info: "Con más de ocho años de experiencia trabajando en agencias de publicidad, departamentos de comunicación.",
        },
        {
          info: "Me considero autodidacta y me gusta pensar “fuera de la caja”.",
        },
        {
          info: "Organizado y con iniciativa, capaz de trabajar bajo presión y con buena actitud en equipo.",
        },
      ],
    },
    education: [
      {
        name: "FULLSTACK DEVELOPER BOOTCAMP",
        date: "2012-2022",
        where: "Upgrade hub",
        logo: "/img/logos/upgrade.png",
      },
      {
        name: "MÁSTER EN ADMINISTRACIÓN DE EMPRESAS",
        date: "2021",
        where: "Instituo Superior Europeo Barcelona",
        logo: "/img/logos/iseb.png",
      },
      {
        name: "ISEB ENGLISH PROGRAM BUSINESS",
        date: "2021",
        where: "Instituo Superior Europeo Barcelona",
        logo: "/img/logos/iseb.png",
      },
      {
        name: "MÁSTER EN COMUNICACIÓN CORPORATIVA",
        date: "2020 -2021",
        where: "Universidad Isabel I Burgos",
        logo: "/img/logos/isabel.jpeg",
      },
      {
        name: "GRADO UNIVERSITARIO DISEÑO GRÁFICO",
        date: "2009 - 2014",
        where: "Escola d’Art i Superior de Disseny de València",
        logo: "/img/logos/easd.jpeg",
      },
      {
        name: "BACHILLERATO BELLAS ARTES",
        date: "2006 - 2008",
        where: "IES Ruiz de Alda",
        logo: "/img/logos/ra.png",
      },
    ],
    experience: [
      {
        name: "AUTÓNOMO",
        date: "2015 – Actualidad",
        where: "Freelance",
        logo: "/img/ojo.gif",
      },
      {
        name: "DISEÑADOR UX/UI",
        date: "2021 – Actualidad",
        where: "Bbrand",
        logo: "/img/logos/bb.png",
      },
      {
        name: "CONSULTOR EN COMUNICACIONES",
        date: "2020 – 2022",
        where: "RACUM Consulting S.L",
        logo: "/img/logos/racum.svg",
      },
      {
        name: "CONSULTOR MARKETING DIGITAL",
        date: "2019 – 2022",
        where: "Organización Iberoamericana de Segurdad Social",
        logo: "/img/logos/oiss.png",
      },
      {
        name: "DISEÑADOR GRÁFICO",
        date: "2018 – 2022",
        where: "Bfun Group",
        logo: "/img/logos/Bfun.png",
      },
      {
        name: "DISEÑADOR GRÁFICO",
        date: "2015 – 2018",
        where: "Mail Boxes Etc",
        logo: "/img/logos/mbe.png",
      },
      {
        name: "DISEÑADOR GRÁFICO [PRÁCTICAS]",
        date: "2015",
        where: "GRAFYCO",
        logo: "/img/logos/g.jpg",
      },
      {
        name: "DISEÑADOR GRÁFICO",
        date: "2012",
        where: "BASTILLA PLAYA",
        logo: "/img/logos/bastilla.jpeg",
      },
      {
        name: "DISEÑADOR GRÁFICO",
        date: "2011",
        where: "ART DECÓ",
        logo: "/img/logos/artdeco.png",
      },
    ],
    languages: [
      {
        language: "Español",
        wrlevel: "Nativo",
        splevel: "Nativo",
      },
      {
        language: "Inglés",
        wrlevel: "Intermedio",
        splevel: "Intermedio",
      },
    ],
    skills: [
      "Gestión de proyetos",
      "Trabajo en equipo",
      "Learnability",
      "Creatividad",
      "Design Thinking",
      "Teletrabajo",
    ],
    cap: [
      "Adobe Creative Suite",
      "Figma",
      "Zeppelin",
      "WordPress",
      "Render 3D",
      "Meta Ads",
      "Google Ads",
      "Photography",
    ],

    tech: ["HTML", "SCSS", "JAVASCRIPT", "ANGULAR", "REACT", "PHP"],

    back: ["MONGO DB", "NODE.JS", "BABEL", "WEBPACK", "SQL", "GIT"],
  },
  en: {
    titles:{
      about: "About me",
    },
    hero: {
      name: "Enrique ",
      subname: "Teruel Gutiérrez",
      city: "Murcia",
      email: "info@enriqueteruel.com",
      birthDate: "13/07/1991",
      phone: "(+34) 639880551",
      image: "/img/ojo.gif",
      gitHub: "@enriqueteru",
      title: "Branding | UX/UI | Full Stack Junior dev",
      aboutMe: [
        {
          info: "My main areas of knowledge are corporate communication, design and marketing, for a few years I have been recycling to unite my two passions, design and programming.",
        },
        {
          info: "With more than eight years of experience working in advertising agencies, communication departments.",
        },
        {
          info: "I consider myself self-taught and like to think “outside the box”",
        },
        {
          info: "Organized and with initiative, able to work under pressure and with a good team attitude.",
        },
      ],
    },
    education: [
      {
        name: "FULLSTACK DEVELOPER BOOTCAMP",
        date: "2012-2022",
        where: "Upgrade hub",
        logo: "/img/logos/upgrade.png",
      },
      {
        name: "MASTER IN BUSINESS ADMINISTRATION",
        date: "2021",
        where: "ISEB",
        logo: "/img/logos/iseb.png",
      },
      {
        name: "ISEB ENGLISH PROGRAM BUSINESS",
        date: "2021",
        where: "ISEB",
        logo: "/img/logos/iseb.png",
      },
      {
        name: "MASTER IN CORPORATE COMMUNICATION",
        date: "2020 -2021",
        where: "Isabel I Burgos University",
        logo: "/img/logos/isabel.jpeg",
      },
      {
        name: "UNIVERSITY DEGREE GRAPHIC DESIGN",
        date: "2009 - 2014",
        where: "School of Art and Superior Design of Valencia",
        logo: "/img/logos/easd.jpeg",
      },
      {
        name: "BACHELOR OF FINE ARTS",
        date: "2006 - 2008",
        where: "IES Ruiz de Alda",
        logo: "/img/logos/ra.png",
      },
    ],
    experience: [
      {
        name: "FREELANCE",
        date: "2015 – nowadays",
        where: "Freelance",
        logo: "/img/ojo.gif",
      },
      {
        name: "UX/UI DESIGNER",
        date: "2021-2022",
        where: "Bbrand",
        logo: "/img/logos/bb.png",
      },
      {
        name: "COMMUNICATIONS CONSULTANT",
        date: "2020 – 2022",
        where: "RACUM Consulting S.L",
        logo: "/img/logos/racum.svg",
      },
      {
        name: "DIGITAL MARKETING EXPERT",
        date: "2019 – 2022",
        where: "Organización Iberoamericana de Segurdad Social",
        logo: "/img/logos/oiss.png",
      },
      {
        name: "GRAPHIC DESIGNER",
        date: "2018 – 2022",
        where: "Bfun Group",
        logo: "/img/logos/Bfun.png",
      },
      {
        name: "GRAPHIC DESIGNER",
        date: "2015 – 2018",
        where: "Mail Boxes Etc",
        logo: "/img/logos/mbe.png",
      },
      {
        name: "GRAPHIC DESIGNER [INTERNSHIPS]",
        date: "2015",
        where: "GRAFYCO",
        logo: "/img/logos/g.jpg",
      },
      {
        name: "GRAPHIC DESIGNER",
        date: "2012",
        where: "BASTILLA PLAYA",
        logo: "/img/logos/bastilla.jpeg",
      },
      {
        name: "GRAPHIC DESIGNER",
        date: "2011",
        where: "ART DECÓ",
        logo: "/img/logos/artdeco.png",
      },
    ],
    languages: [
      {
        language: "Spanish",
        wrlevel: "Native",
        splevel: "Native",
      },
      {
        language: "English",
        wrlevel: "Intermediate",
        splevel: "Intermediate",
      },
    ],
    skills: [
      "Project Management",
      "Teamwork",
      "learnability",
      "Creativity",
      "Design Thinking",
      "Telecommuting"
    ],
    cap: [
      "Adobe Creative Suite",
      "Figma",
      "Zeppelin",
      "WordPress",
      "Render 3D",
      "Meta Ads",
      "Google Ads",
      "Photography",
    ],

    tech: ["HTML", "SCSS", "JAVASCRIPT", "ANGULAR", "REACT", "PHP"],

    back: ["MONGO DB", "NODE.JS", "BABEL", "WEBPACK", "SQL", "GIT"],
  },
};
