import React from "react";

const About = ({ hero: { aboutMe }, titles}) => {
  return (
    <div className="about">
      <div className="about__row">
        <div className="about__row-col">
          <h1 className="item__h1">
            {titles.about}
          </h1>
          {aboutMe.map((item) => {
            return (
              <div className="item" key={JSON.stringify(item)}>
                <p className="item__p"> {item.info}</p>
              </div>
            );
          })}
        </div>
        <div className="about__row-col">
          <img
            className="about__row-img"
            src="/img/about/about-me.jpeg"
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

export default About;
