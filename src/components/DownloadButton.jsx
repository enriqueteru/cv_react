import React from 'react';
import { FormattedMessage as T} from 'react-intl';

const DownloadButton = () => {
  return <button className="absolute button-small" onClick={ev =>  window.location.href='/CV_Enrique_Teruel_2022.pdf'}> <T id="downloadcv" defaultMessage="Descargar CV" /></button>;
};

export default DownloadButton;
