import React from "react";
// import Draggable from 'react-draggable';

const Education = ({ education }) => {
  return (
    <div className="toggle">
      <div className="toggle__row">
        {education.map((item) => {
          return (
            <div className="toggle__card" key={JSON.stringify(item)}>
              <h2 className="toggle__card-title">{item.name}</h2>
              <p className="toggle__card-p">
                {item.date} | {item.where}
              </p>
              <img src={item.logo} alt="" />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Education;

// <div classNameName="education__item" key={JSON.stringify(item)}>
//               <p className="education__item-p" >📕 {item.name}</p>
//               <p className="education__item-p">{item.where}</p>
//               <p className="education__item-p">{item.date}</p>
//             </div>
