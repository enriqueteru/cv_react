import React from "react";

const Experience = ({ experience }) => {
  return (
    <div className="toggle">
      <div className="toggle__row">
        {experience.map((item) => {
          return (
            <div className="toggle__card" key={JSON.stringify(item)}>
              <h2 className="toggle__card-title">{item.name}</h2>
              <p className="toggle__card-p">{item.where}</p>
              <p className="toggle__card-p">{item.date}</p>
              <img src={item.logo} alt="" />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Experience;
