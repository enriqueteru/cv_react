import React from 'react';

const Footer = () => {
   

    const today = new Date();
    const actualYear = today.getFullYear()


  return <div style={{height: '10vh', color: 'white', backgroundColor: 'black', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
     <p>Enrique Teruel Gutiérrez - {actualYear}</p> 
        </div>;
};

export default Footer;
