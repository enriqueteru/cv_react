import React from 'react';


const Hero = ( { hero } ) => {
  return <div className="hero">
   
    <img className="hero__img" alt="ojo" src={hero.image}></img>
    <div className="hero_container">
    <h1 className="hero_container__title"> {hero.name} {hero.subname}</h1>
    <h1 className="hero_container__other"> {hero.email}</h1>
    <h1 className="hero_container__other"> {hero.phone}</h1>
    <h1 className="hero_container__other"> {hero.gitHub}</h1>
    <h1 className="hero_container__other"> {hero.city}</h1>
    </div>
    </div>;
};

export default Hero;
