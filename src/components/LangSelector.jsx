import React from 'react';
import { useContext } from "react";
import  {LangContext} from "../contexts/LangContext";


const LangSelector = () => {
    
    const {locale, changeLanguage} = useContext(LangContext);
  return (
 <div className='langselector'>
  
  <select value={locale} onChange={(ev) => changeLanguage(ev.target.value)}>
      <option value="es">Español</option>
      <option value="en">English</option>
  </select>
</div>
)

};

export default LangSelector;
