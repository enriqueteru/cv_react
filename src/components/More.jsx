import React from "react";

const More = ({ skills, cap, tech, back }) => {
  return (
    <div className="more">
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <h1 className="no-margin">Skills</h1>
        <span className="line"></span>
      </div>

      <div className="more__container ">
        <div className="more__container-col">
          <h2 className="more__title"> Soft skills</h2>
          {skills.map((item, i) => {
            return (
              <div className="more__item" key={JSON.stringify(item)}>
                <p className="more__item-p">{skills[i]}</p>
              </div>
            );
          })}
        </div>
        <div className="more__container-col">
          <h2 className="more__title"> Hard skills</h2>
          {cap.map((item, i) => {
            return (
              <div className="more__item" key={JSON.stringify(item)}>
                <p className="more__item-p">{cap[i]}</p>
              </div>
            );
          })}
        </div>
        <div className="more__container-col">
          <h2 className="more__title"> Front-end</h2>
          {tech.map((item, i) => {
            return (
              <div className="more__item" key={JSON.stringify(item)}>
                <p className="more__item-p">{tech[i]}</p>
              </div>
            );
          })}
        </div>
        <div className="more__container-col">
          <h2 className="more__title"> Back-end</h2>
          {back.map((item, i) => {
            return (
              <div className="more__item" key={JSON.stringify(item)}>
                <p className="more__item-p">{back[i]}</p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default More;
