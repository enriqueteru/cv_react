import React from "react";
import { FormattedMessage as T } from "react-intl";

const Nav = ({ setShowEducation, showEducation }) => {
  return (
    <div className="nav">
      {showEducation === false && (
        <button className="button" onClick={() => setShowEducation(true)}>
          <T id="educationbutton" defaultMessage="Ver educación" />
        </button>
      )}
      {showEducation && (
        <button className="button" onClick={() => setShowEducation(false)}>
          <T id="expbutton" defaultMessage="Ver experiencia" />
        </button>
      )}
    </div>
  );
};

export default Nav;
