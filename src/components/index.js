import About from './About';
import Education from './Education';
import Experience from './Experience';
import Hero from './Hero';
import More from './More';
import Nav from './Nav';
import Footer from './Footer';
import DownloadButton from './DownloadButton';
import LangSelector from './LangSelector';

export {
    About,
    Education,
    Experience,
    Hero,
    More,
    Nav,
    Footer,
    DownloadButton,
    LangSelector,
}