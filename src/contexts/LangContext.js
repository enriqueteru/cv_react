import {createContext, useState} from 'react';
import { IntlProvider } from 'react-intl';
import spanish from '../lang/es.json';
import english from '../lang/en.json';

export const LangContext = createContext();

const navigatorLocale = navigator.language.split('-')[0];

const getMessages = (locale) => {
    if (locale === 'es') return spanish;
    if (locale === 'en') return english;

    return spanish;
};

const LangWrapper = (props) => {
    const [locale, setLocale] = useState(navigatorLocale);
    const [messages, setMessages] = useState(getMessages(navigatorLocale));

    const changeLanguage = (newLanguageLocale) => {
        setLocale(newLanguageLocale);
        setMessages(getMessages(newLanguageLocale))
    };

    return (
        <LangContext.Provider value={{ locale, changeLanguage }}>
            <IntlProvider locale={locale} messages={messages}>
                {props.children}
            </IntlProvider>
        </LangContext.Provider>
    )
}

export default LangWrapper;
