import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import LangWrapper from './contexts/LangContext';



ReactDOM.render(

  <React.StrictMode>
    <LangWrapper>
  <App />
  </LangWrapper>
 </React.StrictMode>,
  document.getElementById('root')
);

